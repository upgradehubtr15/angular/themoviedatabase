import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class BackendServiceService {

  urlMovieDb = 'https://api.themoviedb.org/3';
  apiKey = '0e90da3d1d5b2554b95ae8e121183b3a';
  //url = 'http://localhost:4200/assets/peliculas.json';
  constructor(private http: HttpClient) { }
  
  getMovies() {
    // any[] devuelve un array de cualquier tipo
    const moviedbUrl =  `${this.urlMovieDb}/discover/movie?sort_by=popularity.desc&api_key=${this.apiKey}`;
    return this.http.get<any[]>(moviedbUrl);
  }

  getSingleMovie(movie_id) {
    // any[] devuelve un array de cualquier tipo
    const movieInfo = `${this.urlMovieDb}/movie/${movie_id}?api_key=${this.apiKey}&language=es-ES`
    console.log(movieInfo)
    return this.http.get<any[]>(movieInfo);
  }

  buscarPelicula(busqueda) {
    // any[] devuelve un array de cualquier tipo
    const movieInfo = `https://api.themoviedb.org/3/search/movie?api_key=${this.apiKey}&language=en-US&query=${busqueda}&page=1&include_adult=false`
    return this.http.get<any[]>(movieInfo);
  }

  buscarPorGenero(genero) {
    const movieInfo = `https://api.themoviedb.org/3/discover/movie?api_key=${this.apiKey}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&primary_release_date.gte=1990-01-01&primary_release_date.lte=1999-12-31&vote_average.gte=6&with_genres=${genero}`
    return this.http.get<any[]>(movieInfo);
  }

  getTrailerMovie(movieId) {
    // any[] devuelve un array de cualquier tipo
    
    const movieTrailer =  `http://api.themoviedb.org/3/movie/157336/videos?api_key=${this.apiKey}`;
    return this.http.get<any[]>(movieTrailer);
  }



}
