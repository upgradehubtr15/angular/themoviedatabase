import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';



@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  apiKey = '0e90da3d1d5b2554b95ae8e121183b3a';


  constructor(private router: Router) { }

  ngOnInit() {
  }

  

  iraSearchPeli(slug) {
    console.log("VALUE: "+slug.value.pelicula);
    this.router.navigate(['searchedList', slug.value.pelicula])

  }

  
}
