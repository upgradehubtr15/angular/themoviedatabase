import { Component, OnInit } from '@angular/core';
import { BackendServiceService } from '../services/backend-service.service';
import { ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';



@Component({
  selector: 'app-searched-list',
  templateUrl: './searched-list.component.html',
  styleUrls: ['./searched-list.component.css']
})
export class SearchedListComponent implements OnInit {
  slug: string;
  peliculas: any[] = [];

  constructor(    private _moviesService: BackendServiceService,    private activatedRoute: ActivatedRoute ,    private router: Router    ) 
    {
    this.activatedRoute.params.subscribe(params => {
      this.slug = params['slug'];

      this._moviesService.buscarPelicula(this.slug).subscribe(datos => {
        this.peliculas = [];

        datos['results'].forEach((element)=> {
          console.log(this);
          if (element.backdrop_path !== null) {
            this.peliculas.push(element);
          }
        });        
      })
    })
   }

  ngOnInit() {
  }

  iraPeliBuscada(slug: string) {
    this.router.navigate(['/detail', slug])
  }

}
