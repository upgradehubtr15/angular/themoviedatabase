import { Component, OnInit, Input} from '@angular/core';
import { Router} from '@angular/router';


@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {

  @Input() public peliculas: any[];
  constructor(
    private router: Router

  ) { }

  ngOnInit() {
  }

  iraPeli(slug: string) {
    this.router.navigate(['/detail', slug])
  }


  slideConfig = {"slidesToShow": 4, "slidesToScroll": 4};

}
