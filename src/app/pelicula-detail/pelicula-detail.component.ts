import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { BackendServiceService } from '../services/backend-service.service';


@Component({
  selector: 'app-pelicula-detail',
  templateUrl: './pelicula-detail.component.html',
  styleUrls: ['./pelicula-detail.component.css']
})
export class PeliculaDetailComponent implements OnInit {
  slug: string;
  pelicula:any = {};
  trailers:any = {};
  



   constructor(
    private _moviesService: BackendServiceService,
    private activatedRoute: ActivatedRoute,
    ) 
    {
      this.activatedRoute.params.subscribe(params => {
        this.slug = params['slug'];
      })
      this._moviesService.getSingleMovie(this.slug).subscribe(datos => {
        this.pelicula = datos;
      })
    }



  ngOnInit() {
    this._moviesService.getTrailerMovie("1").subscribe(datos => {
      this.trailers = datos.results[0].key;
      console.log(datos.results[0].key);
    })
  }

}
