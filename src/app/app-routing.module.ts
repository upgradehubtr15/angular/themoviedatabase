import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { PeliculaDetailComponent } from './pelicula-detail/pelicula-detail.component';
import { UserFormComponent } from './user-form/user-form.component';
import { SearchedListComponent } from './searched-list/searched-list.component';

const routes: Routes = [
  {path: 'header', component: HeaderComponent},
  {path: 'home', component: HomeComponent},
  {path: 'detail/:slug', component: PeliculaDetailComponent},
  {path: 'searchedList/:slug', component: SearchedListComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'userform', component: UserFormComponent},
  {path: '**',pathMatch: 'full', redirectTo: 'home'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
