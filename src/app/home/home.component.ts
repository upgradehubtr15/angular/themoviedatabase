import { Component, OnInit } from '@angular/core';
import { BackendServiceService } from '../services/backend-service.service';
import { Router} from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  peliculasPopulares: any = [];
  peliculasAccion: any = [];
  peliculasAventura: any = [];
  movies: any = {};
  
  constructor(private _moviesService: BackendServiceService,
    private router: Router,
    ) {
      this._moviesService.getMovies().subscribe(datos => {
        this.peliculasPopulares = datos['results'];
      })
      this._moviesService.buscarPorGenero("28").subscribe(datos => {
        this.peliculasAccion = datos['results'];
      })
      this._moviesService.buscarPorGenero("12").subscribe(datos => {
        this.peliculasAventura = datos['results'];
      })
    }
    

  ngOnInit() {
  }


}
