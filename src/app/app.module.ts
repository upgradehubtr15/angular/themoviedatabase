import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { ProfileComponent } from './profile/profile.component';
import { HttpClientModule } from '@angular/common/http';
import { BackendServiceService } from './services/backend-service.service';
import { PeliculaDetailComponent } from './pelicula-detail/pelicula-detail.component';
import { UserFormComponent } from './user-form/user-form.component';
import { SearchComponent } from './search/search.component';
import { ListSearchComponent } from './list-search/list-search.component';
import { SearchedListComponent } from './searched-list/searched-list.component';
import { CarouselComponent } from './carousel/carousel.component' ;
import { SlickModule } from 'ngx-slick';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    ProfileComponent,
    PeliculaDetailComponent,
    UserFormComponent,
    SearchComponent,
    ListSearchComponent,
    SearchedListComponent,
    CarouselComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    SlickModule.forRoot()
  ],
  providers: [BackendServiceService],
  bootstrap: [AppComponent],
})
export class AppModule {
 }

